# automation-test-demo
Demo project for UI automation of test cases


As per the requirements, this project focuses on UI automation testing.

Tech stack: Selenium and Java for UI automation, Maven for build management, TestNG for testing, Log4J for logging, JacksonNode for json data manipulation

Setup:

Operating system: Windows 10

Clone the project from git repo.
Project can be imported into eclipse as maven project. All dependencies present in pom.xml will get downloaded by maven.

Execution:
We can run suite or individual scripts from testng.xml present inside resources folder.
OR
It can be run from cmd prompt or terminal using command(From project root directory)
"mvn test"
Note: Maven should be installed and path should be setup in order to run scripts from cmd/terminal

Reporting:
If executing using mvn command from terminal, once suite/test execution is completed, emailable report pr index.html can be checked in target/surefire-reports.
If executing using testng.xml from eclipse, emailable report or index.html can be checked in test-output folder


Browser used for testing: Google Chrome Version 60.0.3112.90 

OS : Windows 10

Default logging level is INFO which can be changed in log4j.xml

Testcases can be executed by running testng.xml as TestNGSuite