package com.demo.listener;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.IAnnotationTransformer2;
import org.testng.annotations.IConfigurationAnnotation;
import org.testng.annotations.IDataProviderAnnotation;
import org.testng.annotations.IFactoryAnnotation;
import org.testng.annotations.ITestAnnotation;

import com.demo.annotation.TestData;
import com.demo.datacontroller.TestDataProvider;
import com.demo.log.Log;

public class TransformerListener implements IAnnotationTransformer2 {

	/** Logger. */
	static Logger log = LogManager.getLogger(TransformerListener.class.getName());

	/** Constant TESTGROUPS: list of valid groups that test must use */
	private static final Set<String> TESTGROUPS = new HashSet<String>(Arrays.asList(new String[] { "stable" }));

	@Override
	@SuppressWarnings("rawtypes")
	public void transform(IConfigurationAnnotation annotation, Class testClass, Constructor testConstructor,
			Method testMethod) {
		Log.logInfo(log, "In Before Annotataion");
	}

	/**
	 * This method might be used in future
	 */
	@Override
	public void transform(IDataProviderAnnotation annotation, Method method) {
	}

	/**
	 * This method might be used in future
	 */
	@Override
	public void transform(IFactoryAnnotation annotation, Method method) {
		// TODO Auto-generated method stub

	}

	/**
	 * Adds data providers to all the tests
	 * 
	 * @see org.testng.IAnnotationTransformer#transform(org.testng.annotations.
	 *      ITestAnnotation, java.lang.Class, java.lang.reflect.Constructor,
	 *      java.lang.reflect.Method)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public void transform(ITestAnnotation anAnnotation, Class aClass, Constructor aConstructor, Method aMethod) {
		if (anAnnotation != null && anAnnotation instanceof ITestAnnotation) {
			if (aMethod.getAnnotation(TestData.class) != null) {
				anAnnotation.setDataProvider("getTestData");
				 anAnnotation.setDataProviderClass(TestDataProvider.class);
			} // if
			String methodName = aMethod.getName();
			String[] groups = anAnnotation.getGroups();
			for (int i = 0; i < groups.length; i++) {
				// If a group found in @Test annotation does not exist in
				// TESTGROUPS, log the group name and exit.
				if (TESTGROUPS.contains(groups[i]) == false) {
					Log.logError(log,"The group: " + groups[i]
							+ " is not an approved group name. Please update the groups in " + methodName + ".");
					System.exit(1);
				} // if
			} // for
			anAnnotation.setGroups(groups);
		} // if
	}// transform

}