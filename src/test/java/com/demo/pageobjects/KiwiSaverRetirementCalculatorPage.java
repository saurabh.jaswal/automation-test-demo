package com.demo.pageobjects;

import static com.demo.util.Util.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.demo.log.Log;
import com.demo.testdata.DataFields;
import com.demo.testdata.DataPaths;
import com.fasterxml.jackson.databind.JsonNode;

public class KiwiSaverRetirementCalculatorPage {
	static Logger log = LogManager.getLogger(KiwiSaverCalculatorsPage.class.getName());
	public WebDriver driver;

	// Help info buttons
	final String currentAgeHelpButton = "//div[@help-id='CurrentAge']/button";
	@FindBy(xpath = currentAgeHelpButton)
	WebElement currentAgeHelpButtonElement;

	final String employementStatusHelpButton = "//div[@help-id='EmploymentStatus']/button";
	@FindBy(xpath = employementStatusHelpButton)
	WebElement employementStatusHelpButtonElement;

	final String pirRateHelpButton = "//div[@help-id='PIRRate']/button";
	@FindBy(xpath = pirRateHelpButton)
	WebElement pirRateHelpButtonElement;

	final String kiwiSaverBalanceHelpButton = "//div[@help-id='KiwiSaverBalance']/button";
	@FindBy(xpath = kiwiSaverBalanceHelpButton)
	WebElement kiwiSaverBalanceHelpButtonElement;

	final String voluntaryContributionsHelpButton = "//div[@help-id='VoluntaryContributions']/button";
	@FindBy(xpath = voluntaryContributionsHelpButton)
	WebElement voluntaryContributionsHelpButtonElement;

	final String riskProfileHelpButton = "//div[@help-id='RiskProfile']/button";
	@FindBy(xpath = riskProfileHelpButton)
	WebElement riskProfileHelpButtonElement;

	final String savingsGoalHelpButton = "//div[@help-id='SavingsGoal']/button";
	@FindBy(xpath = savingsGoalHelpButton)
	WebElement savingsGoalHelpButtonElement;

	// Text box elements
	final String currentAgeHelpText = "//div[@help-id='CurrentAge']//p";
	@FindBy(xpath = currentAgeHelpText)
	WebElement currentAgeHelpTextElement;

	final String empStatusHelpText = "//div[@help-id='EmploymentStatus']//p";
	@FindBy(xpath = empStatusHelpText)
	WebElement empStatusHelpTextElement;

	final String pirRateHelpText = "//div[@help-id='PIRRate']//p";
	@FindBy(xpath = pirRateHelpText)
	WebElement pirRateHelpTextElement;

	final String kiwiSaverBalanceText = "//div[@help-id='KiwiSaverBalance']//p";
	@FindBy(xpath = kiwiSaverBalanceText)
	WebElement kiwiSaverBalanceTextElement;

	final String currentAgeTextBox = "//div[@help-id='CurrentAge']//input";
	@FindBy(xpath = currentAgeTextBox)
	WebElement currentAgeTextBoxElement;

	final String kiwiSaverBalanceTextBox = "//div[@help-id='KiwiSaverBalance']//input";
	@FindBy(xpath = kiwiSaverBalanceTextBox)
	WebElement kiwiSaverBalanceTextBoxElement;

	final String salaryTextBox = "//div[@help-id='AnnualIncome']//input";
	@FindBy(xpath = salaryTextBox)
	WebElement salaryTextBoxElement;

	final String savingsGoalTextBox = "//div[@help-id='SavingsGoal']//input";
	@FindBy(xpath = savingsGoalTextBox)
	WebElement savingsGoalTextBoxElement;

	final String voluntaryContributionTextBox = "//div[@help-id='VoluntaryContributions']//input";
	@FindBy(xpath = voluntaryContributionTextBox)
	WebElement voluntaryContributionTextBoxElement;

	// Drop down values
	final String empStatusDropDown = "//div[@help-id='EmploymentStatus']//i[@class='ir dropdown-arrow']";
	@FindBy(xpath = empStatusDropDown)
	WebElement empStatusDropDownElement;

	final String empStatusEmployed = "//div[@help-id='EmploymentStatus']//child::li[@value='employed']";
	@FindBy(xpath = empStatusEmployed)
	WebElement empStatusEmployedElement;

	final String empStatusSelfEmployed = "//div[@help-id='EmploymentStatus']//child::li[@value='self-employed']";
	@FindBy(xpath = empStatusSelfEmployed)
	WebElement empStatusSelfEmployedElement;

	final String empStatusNotEmployed = "//div[@help-id='EmploymentStatus']//child::li[@value='not-employed']";
	@FindBy(xpath = empStatusNotEmployed)
	WebElement empStatusNotEmployedElement;

	final String kiwiSaverContribution3pcnt = "radio-option-06B";
	@FindBy(id = kiwiSaverContribution3pcnt)
	WebElement kiwiSaverContribution3pcntElement;

	final String kiwiSaverContribution4pcnt = "radio-option-06E";
	@FindBy(id = kiwiSaverContribution4pcnt)
	WebElement kiwiSaverContribution4pcntElement;

	final String kiwiSaverContribution8pcnt = "radio-option-06H";
	@FindBy(id = kiwiSaverContribution8pcnt)
	WebElement kiwiSaverContribution8pcntElement;

	final String voluntaryContributionDropDown = "//div[@help-id='VoluntaryContributions']//i[@class='ir dropdown-arrow']";
	@FindBy(xpath = voluntaryContributionDropDown)
	WebElement voluntaryContributionDropDownElement;

	final String vcStatusOneOff = "//div[@help-id='VoluntaryContributions']//child::li[@value='one-off']";
	@FindBy(xpath = vcStatusOneOff)
	WebElement vcStatusOneOffElement;

	final String vcStatusWeek = "//div[@help-id='VoluntaryContributions']//child::li[@value='week']";
	@FindBy(xpath = vcStatusWeek)
	WebElement vcStatusWeekElement;

	final String vcStatusFortNight = "//div[@help-id='VoluntaryContributions']//child::li[@value='fortnight']";
	@FindBy(xpath = vcStatusFortNight)
	WebElement vcStatusFortNightElement;

	final String vcStatusMonth = "//div[@help-id='VoluntaryContributions']//child::li[@value='month']";
	@FindBy(xpath = vcStatusMonth)
	WebElement vcStatusMonthElement;

	final String vcStatusYear = "//div[@help-id='VoluntaryContributions']//child::li[@value='year']";
	@FindBy(xpath = vcStatusYear)
	WebElement vcStatusYearElement;

	final String pirRateDropDown = "//div[@help-id='PIRRate']//i[@class='ir dropdown-arrow']";
	@FindBy(xpath = pirRateDropDown)
	WebElement pirRateDropDownElement;

	final String pirRateValueLow = "//div[@help-id='PIRRate']//child::li[@value='10.5']";
	@FindBy(xpath = pirRateValueLow)
	WebElement pirRateValueLowElement;

	final String pirRateValueMedium = "//div[@help-id='PIRRate']//child::li[@value='17.5']";
	@FindBy(xpath = pirRateValueMedium)
	WebElement pirRateValueMediumElement;

	final String pirRateValueHigh = "//div[@help-id='PIRRate']//child::li[@value='28']";
	@FindBy(xpath = pirRateValueHigh)
	WebElement pirRateValueHighElement;

	final String riskProfileLow = "radio-option-01V";
	@FindBy(id = riskProfileLow)
	WebElement riskProfileLowElement;

	final String riskProfileMedium = "radio-option-01Y";
	@FindBy(id = riskProfileMedium)
	WebElement riskProfileMediumElement;

	final String riskProfileHigh = "radio-option-021";
	@FindBy(id = riskProfileHigh)
	WebElement riskProfileHighElement;

	// Calculate projections element
	final String calculateProjectionButton = "//button[./span[text()='View your KiwiSaver retirement projections']]";
	@FindBy(xpath = calculateProjectionButton)
	WebElement calculateProjectionButtonElement;

	// Result element
	final String result = "//span[./span[@class='prefix'  and text()='$']]";
	@FindBy(xpath = result)
	WebElement resultElement;

	final String iFrameCalculator = "//iframe[@src='/calculator-widgets/kiwisaver-calculator/?gclid=&referrer=https%3A%2F%2Fwww.westpac.co.nz%2F&parent=3956&host=calculator-embed']";

	public KiwiSaverRetirementCalculatorPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Verify all info buttons are present
	 */
	public void verifyAllInfoButtonsPresent() {
		switchToFrame(driver, By.xpath(iFrameCalculator));
		waitForElementToBeVisible(By.xpath(savingsGoalHelpButton), driver);
		waitForElementToBeClickable(By.xpath(savingsGoalHelpButton), driver);
		if (isDisplayedAndEnabled(currentAgeHelpButtonElement, driver)
				&& isDisplayedAndEnabled(employementStatusHelpButtonElement, driver)
				&& isDisplayedAndEnabled(pirRateHelpButtonElement, driver)
				&& isDisplayedAndEnabled(kiwiSaverBalanceHelpButtonElement, driver)
				&& isDisplayedAndEnabled(voluntaryContributionsHelpButtonElement, driver)
				&& isDisplayedAndEnabled(riskProfileHelpButtonElement, driver)
				&& isDisplayedAndEnabled(savingsGoalHelpButtonElement, driver)) {
			Log.logInfo(log, "All information buttons are present and enabled for calculator");
		} else
			Log.logError(log, "Information buttons not present");
		switchToDefault(driver);

	}

	/**
	 * Verify info button text value
	 */
	public void verifyInfoButtonText(String key, String value) {
		switchToFrame(driver, By.xpath(iFrameCalculator));
		switch (key) {
		case DataPaths.AGE_HELPINFO:
			waitForElementToBeClickable(By.xpath(currentAgeHelpButton), driver);
			clickOnWebElement(currentAgeHelpButtonElement, driver);
			waitForElementToBeVisible(By.xpath(currentAgeHelpText), driver);
			Assert.assertEquals(currentAgeHelpTextElement.getText(), value);
			Log.logInfo(log, "Current age help info text present");
			switchToDefault(driver);
			break;

		case DataPaths.KIWI_SAVERBALANCE_INFO:
			waitForElementToBeClickable(By.xpath(kiwiSaverBalanceHelpButton), driver);
			clickOnWebElement(kiwiSaverBalanceHelpButtonElement, driver);
			waitForElementToBeVisible(By.xpath(kiwiSaverBalanceText), driver);
			Assert.assertEquals(kiwiSaverBalanceTextElement.getText(), value);
			Log.logInfo(log, "Kiwisaver help info text present");
			switchToDefault(driver);
			break;

		default:
			switchToDefault(driver);
			Log.logError(log, "Invalid option");
			break;
		}
	}

	/**
	 * Calculate kiwi saver projection balance
	 */
	public void calculateKiwiSaverProjectionBalance(JsonNode node) {

		// Switch to calculator frame
		switchToFrame(driver, By.xpath(iFrameCalculator));
		waitForElementToBeClickable(By.xpath(currentAgeTextBox), driver);

		// Enter Age
		enterData(currentAgeTextBoxElement, node.get(DataFields.AGE_FIELD.field()).asText());
		Log.logInfo(log, "Entered age value :" + node.get(DataFields.AGE_FIELD.field()).asText());

		// Click on employment status drop down button
		clickOnWebElement(empStatusDropDownElement, driver);
		Log.logInfo(log, "Clicked on Employment status drop down button");

		if (node.get(DataFields.EMPLOYEMENT_STATUS_FIELD.field()).asText().equals(DataFields.EMPLOYED_VALUE.field())) {

			// Select Employed option
			clickOnWebElement(empStatusEmployedElement, driver);
			Log.logInfo(log, "Selected employment status as Employed");

			// Enter salary
			waitForElementToBeClickable(By.xpath(salaryTextBox), driver);
			enterData(salaryTextBoxElement, node.get(DataFields.SALARY_FIELD.field()).asText());
			Log.logInfo(log, "Entered salary :" + node.get(DataFields.SALARY_FIELD.field()).asText());

			// Select kiwi saver value based on input data
			switch (node.get(DataFields.KIWISAVER_FIELD.field()).asText()) {
			case DataPaths.KIWI_SAVER_VALUE_3:
				clickOnWebElement(kiwiSaverContribution3pcntElement, driver);
				break;
			case DataPaths.KIWI_SAVER_VALUE_4:
				clickOnWebElement(kiwiSaverContribution4pcntElement, driver);
				break;
			case DataPaths.KIWI_SAVER_VALUE_8:
				clickOnWebElement(kiwiSaverContribution8pcntElement, driver);
				break;

			default:
				Log.logError(log, "Invalid value for kiwi Saver contribution");
				break;
			}
			Log.logInfo(log, "Selected kiwi saver member contribution as :"
					+ node.get(DataFields.KIWISAVER_FIELD.field()).asText());
		}

		// Select employment status as self employed
		else if (node.get(DataFields.EMPLOYEMENT_STATUS_FIELD.field()).asText()
				.equals(DataFields.SELF_EMPLOYED_VALUE.field())) {
			// Select Employed option
			clickOnWebElement(empStatusSelfEmployedElement, driver);
			Log.logInfo(log, "Selected Employment status as Self employed");
		}

		// Select employment status as not employment
		else if (node.get(DataFields.EMPLOYEMENT_STATUS_FIELD.field()).asText()
				.equals(DataFields.NOT_EMPLOYED_STATUS_VALUE.field())) {
			// Select Employed option
			clickOnWebElement(empStatusNotEmployedElement, driver);
			Log.logInfo(log, "Selected Employment status as Not employed");
		}

		// Select PIR rate

		if (!node.get(DataFields.PIRRATE_FIELD.field()).isNull()) {
			// Click on PIR dropdown
			clickOnWebElement(pirRateDropDownElement, driver);
			Log.logInfo(log, "Clicked pirRate drop down button");

			switch (node.get(DataFields.PIRRATE_FIELD.field()).asText()) {
			case DataPaths.PIR_RATE_LOW:
				waitForElementToBeClickable(By.xpath(pirRateValueLow), driver);
				clickOnWebElement(pirRateValueLowElement, driver);
				break;

			case DataPaths.PIR_RATE_MEDIUM:
				waitForElementToBeClickable(By.xpath(pirRateValueMedium), driver);
				clickOnWebElement(pirRateValueMediumElement, driver);
				break;

			case DataPaths.PIR_RATE_HIGH:
				waitForElementToBeClickable(By.xpath(pirRateValueHigh), driver);
				clickOnWebElement(pirRateValueHighElement, driver);
				break;

			default:
				Log.logError(log, "Invalid option for pir rate");
				break;
			}
			Log.logInfo(log, "Clicked pirRate value as :" + node.get(DataFields.PIRRATE_FIELD.field()).asText());
		}

		// Enter kiwi saver balance(if applicable)
		if (!node.get(DataFields.CURRENT_KIWISAVERBALANCE_FIELD.field()).isNull()) {
			waitForElementToBeClickable(By.xpath(kiwiSaverBalanceTextBox), driver);
			enterData(kiwiSaverBalanceTextBoxElement,
					node.get(DataFields.CURRENT_KIWISAVERBALANCE_FIELD.field()).asText());
			Log.logInfo(log, "Entered kiwisaver balance :"
					+ node.get(DataFields.CURRENT_KIWISAVERBALANCE_FIELD.field()).asText());
		}

		// Enter voluntary contributions(if applicable)
		if (!node.get(DataFields.VOLUNTARY_CONTRIBUTIONVALUE_FIELD.field()).isNull()) {
			waitForElementToBeClickable(By.xpath(voluntaryContributionTextBox), driver);
			enterData(voluntaryContributionTextBoxElement,
					node.get(DataFields.VOLUNTARY_CONTRIBUTIONVALUE_FIELD.field()).asText());

			Log.logInfo(log, "Entered voluntary contribution :"
					+ node.get(DataFields.VOLUNTARY_CONTRIBUTIONVALUE_FIELD.field()).asText());

			clickOnWebElement(voluntaryContributionDropDownElement, driver);

			switch (node.get(DataFields.VOLUNTARY_CONTRIBUTIONFREQUENCY_FIELD.field()).asText()) {

			case DataPaths.VC_FREQUENCY_VALUE_ONEOFF:
				waitForElementToBeClickable(By.xpath(vcStatusOneOff), driver);
				clickOnWebElement(vcStatusOneOffElement, driver);
				break;

			case DataPaths.VC_FREQUENCY_VALUE_WEEKLY:
				waitForElementToBeClickable(By.xpath(vcStatusWeek), driver);
				clickOnWebElement(vcStatusWeekElement, driver);
				break;

			case DataPaths.VC_FREQUENCY_VALUE_FORTNIGHTLY:
				waitForElementToBeClickable(By.xpath(vcStatusFortNight), driver);
				clickOnWebElement(vcStatusFortNightElement, driver);
				break;

			case DataPaths.VC_FREQUENCY_VALUE_MONTHLY:
				waitForElementToBeClickable(By.xpath(vcStatusMonth), driver);
				clickOnWebElement(vcStatusMonthElement, driver);
				break;

			case DataPaths.VC_FREQUENCY_VALUE_ANNUALLY:
				waitForElementToBeClickable(By.xpath(vcStatusYear), driver);
				clickOnWebElement(vcStatusYearElement, driver);
				break;

			default:
				Log.logError(log, "Invalid option for voluntary frequency value");
				break;
			}
			Log.logInfo(log, "Selected voluntary contribution value as :"
					+ node.get(DataFields.VOLUNTARY_CONTRIBUTIONFREQUENCY_FIELD.field()).asText());
		}

		// Select risk profile
		switch (node.get(DataFields.RISK_FIELD.field()).asText()) {
		case DataPaths.RISK_PROFILE_LOW:
			clickOnWebElement(riskProfileLowElement, driver);
			break;
		case DataPaths.RISK_PROFILE_MEDIUM:
			clickOnWebElement(riskProfileMediumElement, driver);
			break;
		case DataPaths.RISK_PROFILE_HIGH:
			clickOnWebElement(riskProfileHighElement, driver);
			break;

		default:
			Log.logError(log, "Invalid value for kiwi Saver contribution");
			break;
		}
		Log.logInfo(log, "Selected risk profile value as :" + node.get(DataFields.RISK_FIELD.field()).asText());

		// Enter Savings goal
		if (!node.get(DataFields.SAVINGSGOAL_FIELD.field()).isNull()) {
			waitForElementToBeClickable(By.xpath(savingsGoalTextBox), driver);
			enterData(savingsGoalTextBoxElement, node.get(DataFields.SAVINGSGOAL_FIELD.field()).asText());
			Log.logInfo(log,
					"Entered savings goal value as :" + node.get(DataFields.SAVINGSGOAL_FIELD.field()).asText());
		}

		clickOnWebElement(calculateProjectionButtonElement, driver);
		Log.logInfo(log, "Clicked on calculate projection button");

		waitForElementToBePresent(By.xpath(result), driver);
		Log.logInfo(log, "At age " + node.get(DataFields.AGE_FIELD.field()) + ",estimated Kiwisaver balance is :"
				+ formatResult(resultElement.getText()));

		switchToDefault(driver);
	}
}
