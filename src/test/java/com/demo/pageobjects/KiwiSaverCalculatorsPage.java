package com.demo.pageobjects;

import static com.demo.util.Util.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.demo.log.Log;

public class KiwiSaverCalculatorsPage {
	static Logger log = LogManager.getLogger(KiwiSaverCalculatorsPage.class.getName());

	public WebDriver driver;

	final String getStartedButton = "Click here to get started.";
	@FindBy(linkText = getStartedButton)
	WebElement getStartedButtonElement;

	public KiwiSaverCalculatorsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Click on get started link
	 */
	public void clickOnGetStarted() {
		waitForElementToBeVisible(By.linkText(getStartedButton), driver);
		waitForElementToBeClickable(By.linkText(getStartedButton), driver);
		clickOnWebElement(getStartedButtonElement, driver);
		Log.logInfo(log, "Clicked on get started button ");
	}
}
