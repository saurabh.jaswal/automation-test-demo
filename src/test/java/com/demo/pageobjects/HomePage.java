package com.demo.pageobjects;

import static com.demo.util.Util.*;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.demo.log.Log;

public class HomePage {
	static Logger log = LogManager.getLogger(HomePage.class.getName());
	public WebDriver driver;

	// Page web elements
	final String kiwiSaverButton = "ubermenu-section-link-kiwisaver-ps";
	@FindBy(id = kiwiSaverButton)
	WebElement kiwiSaverButtonElement;

	final String kiwiSaverCalculator = "ubermenu-item-cta-kiwisaver-calculators-ps";
	@FindBy(id = kiwiSaverCalculator)
	WebElement kiwiSaverCalculatorElement;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Hover over Kiwi saver
	 */
	public void hoverOverKiwiSaver() {
		boolean returnValue = false;
		int counter = 0;
		waitForElementToBePresent(By.id(kiwiSaverButton), driver);
		Actions builder = new Actions(driver);
		while (!returnValue && counter <= 3) {
			builder.moveToElement(kiwiSaverButtonElement).build().perform();
			// Verify that element is enabled
			waitForElementToBeClickable(By.id(kiwiSaverCalculator), driver);
			if (isEnabled(kiwiSaverCalculatorElement, driver)) {
				returnValue = true;
				Log.logInfo(log, "Hovered over Kiwi Saver Button");
			} else
				returnValue = false;
			builder.release();
			counter++;
		}
		if (!returnValue)
			Log.logError(log, "Not able to hover over kiwi saver button");
	}

	/**
	 * Click on kiwiSaver calculators
	 */
	public void clickKiwiSaverCalculators() {
		waitForElementToBeClickable(By.id(kiwiSaverCalculator), driver);
		if (isEnabled(kiwiSaverCalculatorElement, driver)) {
			clickOnWebElement(kiwiSaverCalculatorElement, driver);
			Log.logInfo(log, "Clicked on kiwi saver calculators");
		}
	}
}
