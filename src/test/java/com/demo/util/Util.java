package com.demo.util;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.demo.log.Log;

public class Util {
	static Logger log = LogManager.getLogger(Util.class.getName());

	/**
	 * Switch between windows
	 * 
	 * @param handles
	 * @param driver
	 */
	public static void switchHandles(Set<String> handles, WebDriver driver) {
		for (String window : handles) {
			if (!(window.equals(driver.getWindowHandle())))
				driver.switchTo().window(window);
			Log.logInfo(log, "Switched to window :" + window);
		}
	}

	/**
	 * To check for whether web element is displayed or not
	 * 
	 * @param element
	 *            to check for it display
	 * @param appiumDriver
	 *            the SwipeableWebDriver instance
	 */
	public static boolean isDisplayed(WebElement element, WebDriver appiumDriver) {
		try {
			if (element.isDisplayed()) {
				Log.logDebug(log, "WebElement " + element.toString() + " displayed");
				return true;
			}
			return false;
		} // try
		catch (Exception e) {
			return false;
		} // catch
	}// isDisplayed

	/**
	 * To check for whether web element is enabled or not
	 * 
	 * @param element
	 *            to check if is enabled
	 */
	public static boolean isEnabled(WebElement element, WebDriver appiumDriver) {
		try {
			if (element.isEnabled()) {
				Log.logDebug(log, "WebElement " + element.toString() + " enabled");
				return true;
			}
			return false;
		} // try
		catch (Exception e) {
			return false;
		} // catch
	}// isDisplayed

	/**
	 * This method is used to click on the given element. It tries to click the
	 * element by using basic click method given for WebElement. If it throws
	 * exception, it tries to click by using JavaScript
	 * 
	 * 
	 * @param element
	 * @param driver
	 */
	public static void clickOnWebElement(WebElement element, WebDriver driver) {
		String getElementLocator = null;
		try {
			element.click();
			Log.logDebug(log, "WebElement " + element.toString() + " clicked");
		} catch (Exception e) {
			try {
				String getElementLocatorTemp = element.toString().split("->")[1];
				getElementLocator = getElementLocatorTemp.substring(0, (getElementLocatorTemp.length() - 1));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click()", element);
				Log.logDebug(log, "WebElement " + element.toString() + " clicked using JavaScript Executor");
			} catch (Exception e2) {
				Log.logErrorEx(log, "clickOnWebElement() -> Web Element not clicked -> " + getElementLocator, e2);
			}
		}

	}// clickOnWebElement

	/**
	 * 
	 * Using webdriver wait, wait for particular element to be clickable by
	 * using any of the locator strategy used in org.openqa.selenium.By Default
	 * time out is 60 Secs
	 * 
	 * @param by
	 * @param driver
	 */
	public static void waitForElementToBeClickable(By by, WebDriver driver) {
		try {
			Log.logDebug(log, "Waiting for element to be clickable by locator :" + by.toString());
			new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while waitForElementToBePresentById ", e);
		}

	}

	/**
	 * Using webdriver wait, wait for particular element to be visible by using
	 * any of the locator strategy used in org.openqa.selenium.By Default
	 * timeout is 60 secs
	 * 
	 * @param by
	 * @param driver
	 */
	public static void waitForElementToBeVisible(By by, WebDriver driver) {
		try {
			Log.logDebug(log, "Waiting for element to be visible by locator :" + by.toString());
			new WebDriverWait(driver, 30).pollingEvery(5, TimeUnit.MICROSECONDS)
			.until(ExpectedConditions.visibilityOfElementLocated(by));
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while waitForElementToBePresentByXpath ", e);
		}
	}

	/**
	 * Using webdriver wait, wait for particular element to be present by using
	 * any of the locator strategy used in org.openqa.selenium.By Default
	 * timeout is 60 secs
	 * 
	 * @param by
	 * @param driver
	 */
	public static void waitForElementToBePresent(By by, WebDriver driver) {
		try {
			Log.logDebug(log, "Waiting for element to be present by locator :" + by.toString());
			new WebDriverWait(driver, 30).pollingEvery(5, TimeUnit.MICROSECONDS)
			.until(ExpectedConditions.presenceOfElementLocated(by));
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while waitForElementToBePresentByXpath ", e);
		}
	}

	/**
	 * 
	 * Switches to given frame
	 * 
	 * @param driver
	 * @param by
	 */
	public static void switchToFrame(WebDriver driver, By by) {
		Log.logDebug(log, "Switching to frame with locator :" + by.toString());
		new WebDriverWait(driver, 30).pollingEvery(5, TimeUnit.MICROSECONDS)
		.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
	}

	/**
	 * Switches to default content on DOM
	 * 
	 * @param driver
	 */
	public static void switchToDefault(WebDriver driver) {
		Log.logDebug(log, "Switching to default content");
		driver.switchTo().defaultContent();
	}

	/**
	 * 
	 * Checks whether element is displayed and enabled inside DOM
	 * 
	 * @param element
	 * @param appiumDriver
	 * @return
	 */
	public static boolean isDisplayedAndEnabled(WebElement element, WebDriver appiumDriver) {
		try {
			if (element.isDisplayed() && element.isEnabled()) {
				Log.logDebug(log, "WebElement " + element.toString() + " displayed and enabled");
				return true;
			}
			return false;
		} // try
		catch (Exception e) {
			return false;
		} // catch
	}// isDisplayedAndEnabled

	public static void enterData(WebElement element, String text) {
		try {
			Log.logDebug(log, "Sending data :" + text + "to element :" + element.toString());
			element.sendKeys(text);
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while entering data in : " + element.toString(), e);
		}
	}
	
	public static String formatResult(String text){
		return text.replaceAll("[^0-9?!\\.]","");
	}
}
