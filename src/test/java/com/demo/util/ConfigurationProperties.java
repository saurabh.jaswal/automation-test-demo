package com.demo.util;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.demo.log.Log;

public class ConfigurationProperties {

	private ConfigurationProperties() {

	}

	private static ConfigurationProperties getInstance = null;

	public static synchronized ConfigurationProperties getInstance() {
		if (getInstance == null) {
			synchronized (ConfigurationProperties.class) {
				if (getInstance == null) {
					getInstance = new ConfigurationProperties();
				}
			}
		}
		return getInstance;
	}

	static Logger log = LogManager.getLogger(ConfigurationProperties.class.getName());
	private static Properties properties = new Properties();

	static {
		try {
			properties.load(ConfigurationProperties.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while loading properties: ", e);
		}
	}

	public synchronized String getProperty(String key) {
		String value = properties.getProperty(key);
		if (value.length() == 0) {
			Log.logError(log, "No value present  for given key");
		}
		return value;
	}
}
