package com.demo.base;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.demo.log.Log;
import com.demo.util.ConfigurationProperties;
import com.fasterxml.jackson.databind.JsonNode;

public class BaseTest{
	static Logger log = LogManager.getLogger(BaseTest.class.getName());
	static DriverFactory driverFactory = new DriverFactory();
	static WebDriver driver ;

	/** The test method. */
	protected Method testMethod = null;
	
	/**
	 * @returns an instance of webdriver based on browser choice
	 */
	public static WebDriver getWebDriver(){
		if(driver == null)
			driver =  DriverFactory.getWebDriver(Integer.parseInt(ConfigurationProperties.getInstance().getProperty("browser").trim()));
		return driver;
	}

	/**
	 * Method launches the base URL present in config file
	 * @return true if URL is launched and window is maximized else false
	 */
	public boolean launchBaseURL(){
		try{
			driver.get(ConfigurationProperties.getInstance().getProperty("baseurl"));
			driver.manage().window().maximize();
			return true;	
		}catch(Exception e ){
			Log.logErrorEx(log,"Error : " , e);
			return false;
		}

	}

	@BeforeSuite(alwaysRun = true)
	public void setup(ITestContext context){
		Log.logInfo(log,"===========================================Starting the suite "+context.getSuite().getName()+"  ===============================================================");
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setup(Method aTestMethod) {
		this.testMethod = aTestMethod;
		if (aTestMethod.getAnnotation(Test.class) == null) {
			Log.logInfo(log,"==========================================================================================================");
			Log.logInfo(log,"The test method is not correctly annotated. It should always have @Test annotations.");
			Log.logInfo(log,"==========================================================================================================");
		}// if
	}// setup

	@AfterClass(alwaysRun = true)
	public void quitDriver(){
		driver.quit();
		driver = null;
		
	}
	
	@AfterSuite(alwaysRun = true)
	public void closeSuite(){
		Log.logInfo(log,"===========================================Closing the suite===============================================================");
	}

	public JsonNode unpackData(ArrayList<JsonNode> data){
		if(data.isEmpty())
			return null;
		else
			return data.get(0);
	}
}
