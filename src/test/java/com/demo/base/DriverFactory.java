package com.demo.base;

import java.io.File;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import com.demo.log.Log;

public class DriverFactory {

	static Logger log = LogManager.getLogger(DriverFactory.class.getName());
	static String fileSep = File.separator;
	public static WebDriver getWebDriver(int browser) {

		switch (browser) {
		case 1:
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + fileSep+"src"+fileSep+"test"+fileSep+"resources"+fileSep+"browsers"+fileSep+"geckodriver.exe");
			return new SafariDriver(DesiredCapabilities.safari());
		case 2:
			return new FirefoxDriver(DesiredCapabilities.firefox());
		case 3:
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + fileSep+"src"+fileSep+"test"+fileSep+"resources"+fileSep+"browsers"+fileSep+"chromedriver.exe");
			return new ChromeDriver(DesiredCapabilities.chrome());
		default:
			Log.logError(log, "Invalid option");
			return null;
		}
	}
}
