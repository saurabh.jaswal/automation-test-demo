package com.demo.datacontroller;
public interface TestDataReader {

	/**
	 * Retrieves the test data from an external file.
	 * 
	 * @param aDataSource - the name of the data source
	 * @return Object - the test data
	 * @throws Exception - exception when reading the file
	 **/
	public Object getTestData(String aDataSource)
			throws Exception;

}
