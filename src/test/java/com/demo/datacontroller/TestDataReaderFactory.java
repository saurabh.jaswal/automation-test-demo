package com.demo.datacontroller;


public class TestDataReaderFactory {

	/**
	 * Assigns the data reader based on source type.
	 * 
	 * @param aDataSourceType - data source type
	 * @return TestDataReader - instance of TestDataReader
	 **/
	public static TestDataReader getTestDataReader(String aDataSourceType) {
		TestDataReader testDataReader;
		if (aDataSourceType.equals("JSON")) {
			testDataReader = new JsonTestDataReader();
		} else {
			testDataReader = null;
		}
		return testDataReader;
	}

}
