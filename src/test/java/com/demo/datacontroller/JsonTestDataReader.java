package com.demo.datacontroller;

import java.io.InputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.demo.log.Log;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonTestDataReader implements TestDataReader {

	/** Logger Instance **/
	static Logger log = LogManager.getLogger(JsonTestDataReader.class.getName());

	public static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Retrieves the JSON contained in the flat file and converts it into a
	 * JsonNode.
	 *
	 * @param jsonStream
	 *            - an input stream
	 * @return Object - a JSON object representing the input data
	 **/
	private static Object getFromJSONCollection(InputStream jsonStream) {
		try {
			return mapper.readTree(jsonStream);
		} catch (Exception e) {
			Log.logErrorEx(log, "Error while reading file ", e);
		}
		return null;
	}

	/**
	 * Opens an input stream to the resource .json file passed in, located
	 * within the /data directory, and retrieves the data in this file. This
	 * method is used by the DataProvider.
	 *
	 * @param aDataSource
	 *            - the file name of the data source
	 * @return Object - the input data
	 **/
	@Override
	public Object getTestData(String aDataSource) throws Exception {
		Object testData = null;
		try {
			InputStream jsonStream = JsonTestDataReader.class.getClassLoader()
					.getResourceAsStream(aDataSource + ".json");
			testData = getFromJSONCollection(jsonStream);
		} catch (Exception e) {
			Log.logFatalEx(log, "Error reading json test data file : ", e);
			throw e;
		}
		return testData;
	}

	/**
	 * Opens an input stream to the .json file passed in, located within the
	 * /resource directory, and retrieves the contents of this file
	 *
	 * @param aDataSource
	 *            - the file name of the data source
	 * @return JsonNode - the data object
	 **/
	public static synchronized JsonNode getJsonFile(String aDataSource) {
		JsonNode testData = null;
		try {
			InputStream jsonStream = JsonTestDataReader.class.getClassLoader().getResourceAsStream(aDataSource);
			testData = (JsonNode) getFromJSONCollection(jsonStream);
		} catch (Exception e) {
			Log.logFatalEx(log, "Exception when reading .json file <" + aDataSource + "> : ", e);
		}
		return testData;
	}

}