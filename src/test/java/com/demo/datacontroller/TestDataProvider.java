package com.demo.datacontroller;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

import com.demo.annotation.TestData;
import com.demo.log.Log;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class TestDataProvider {

	/** Logger Instance **/
	static Logger log = LogManager.getLogger(TestDataProvider.class.getName());

	/**
	 * Retrieves the test data for a test annotated with: @TestData
	 * The entire input is saved as a JsonNode object and added to the Object[][]
	 * 
	 * @return Object[][] - Returns single index containing a JsonNode object
	 **/
	@DataProvider(name = "getTestData")
	public static Object[][] getTestData(Method aMethod) {

		Object[][] returnArray = null;
		try {
			Annotation[] testMethodAnnotations = aMethod.getAnnotations();
			String sourceName = "";
			String sourceType = "";
			for (Annotation annotation : testMethodAnnotations) {
				if (annotation instanceof TestData) {
					TestData testDataAnnotation = (TestData) annotation;
					sourceType = testDataAnnotation.type();
					sourceName = testDataAnnotation.source();
				}
			}
			if ("".equals(sourceName) || "".equals(sourceType)) {
			} else {
				
				// In case OS is MAC or linux
				sourceName = sourceName.replaceAll("\\/ ", File.separator);
				ArrayList<Object> testData = new ArrayList<Object>();
				testData.add(TestDataReaderFactory.getTestDataReader(sourceType)
						.getTestData(sourceName));
				if(testData.get(0) instanceof ArrayNode){
					JsonNode arr = ((JsonNode)testData.get(0));
					returnArray = new Object[arr.size()][1];
					for(int i = 0; i < arr.size() ; i++){
						returnArray[i][0] = arr.get(i);
					}
				}
				else{
					returnArray = new Object[1][1];
					returnArray[0][0] = testData;
				}
			}
		} catch (Exception e) {
			Log.logFatalEx(log, "Error reading test data file : ", e);
		}
		return returnArray;
	}
}