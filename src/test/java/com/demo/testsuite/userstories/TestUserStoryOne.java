package com.demo.testsuite.userstories;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.demo.annotation.TestData;
import com.demo.base.BaseTest;
import com.demo.pageobjects.HomePage;
import com.demo.pageobjects.KiwiSaverCalculatorsPage;
import com.demo.pageobjects.KiwiSaverRetirementCalculatorPage;
import com.demo.testdata.DataPaths;
import com.fasterxml.jackson.databind.JsonNode;

public class TestUserStoryOne extends BaseTest {
	static Logger log = LogManager.getLogger(TestUserStoryOne.class.getName());

	@Test(groups = "stable")
	@TestData(source = DataPaths.STORY_ONE, type = DataPaths.JSON)
	public void verifyInfoIconPresentForAllFields(ArrayList<JsonNode> input) {
		JsonNode data = unpackData(input);
		HomePage homePage = new HomePage(getWebDriver());
		KiwiSaverCalculatorsPage calculatorsPage = new KiwiSaverCalculatorsPage(getWebDriver());
		KiwiSaverRetirementCalculatorPage retirementCalculatorPage = new KiwiSaverRetirementCalculatorPage(getWebDriver());
		
		// Launch Base URL
		launchBaseURL();
		
		// Hover Over Kiwi saver button
		homePage.hoverOverKiwiSaver();
		
		// Click on kiwi saver calculators link
		homePage.clickKiwiSaverCalculators();
		
		// Click on get started link
		calculatorsPage.clickOnGetStarted();
		
		// Verify All Info buttons are present in calculator
		retirementCalculatorPage.verifyAllInfoButtonsPresent();
		
		// Verify value for Info buttons 
		Iterator<Entry<String, JsonNode>> iterator = data.fields();
		while (iterator.hasNext()) {
			Entry<String, JsonNode> entry = (Entry<String, JsonNode>) iterator.next();
			retirementCalculatorPage.verifyInfoButtonText(entry.getKey(), entry.getValue().asText());
		}

	}//verifyInfoIconPresentForAllFields

}
