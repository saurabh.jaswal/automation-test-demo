package com.demo.testsuite.userstories;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.demo.annotation.TestData;
import com.demo.base.BaseTest;
import com.demo.pageobjects.HomePage;
import com.demo.pageobjects.KiwiSaverCalculatorsPage;
import com.demo.pageobjects.KiwiSaverRetirementCalculatorPage;
import com.demo.testdata.DataPaths;
import com.fasterxml.jackson.databind.JsonNode;

public class TestUserStoryTwo extends BaseTest {
	static Logger log = LogManager.getLogger(TestUserStoryTwo.class.getName());

	@Test(groups = "stable")
	@TestData(source = DataPaths.STORY_TWO, type = DataPaths.JSON)
	public void verifyUserAbleToCalculateProjectedBalance(JsonNode input) {
		JsonNode data = input;
		HomePage homePage = new HomePage(getWebDriver());
		KiwiSaverCalculatorsPage calculatorsPage = new KiwiSaverCalculatorsPage(getWebDriver());
		KiwiSaverRetirementCalculatorPage retirementCalculatorPage = new KiwiSaverRetirementCalculatorPage(getWebDriver());
		
		// Launch URL
		launchBaseURL();
		
		// Hover over kiwi saver button
		homePage.hoverOverKiwiSaver();
		
		// Click on kiwi saver calculators
		homePage.clickKiwiSaverCalculators();
		
		// Click on get started link
		calculatorsPage.clickOnGetStarted();
		
		// Calculate kiwiSaver projected balance for multiple data sets
		retirementCalculatorPage.calculateKiwiSaverProjectionBalance(data);

	}//verifyUserAbleToCalculateProjectedBalance

}
