package com.demo.testdata;


public enum DataFields{
	
	EMPLOYEMENT_STATUS_FIELD("employementStatus"),
	EMPLOYED_VALUE("Employed"),
	SELF_EMPLOYED_VALUE("Self-employed"),
	NOT_EMPLOYED_STATUS_VALUE("Not-employed"),
	AGE_FIELD("age"),
	SALARY_FIELD("salary"),
	KIWISAVER_FIELD("kiwiSaver"),
	PIRRATE_FIELD("pirRate"),
	CURRENT_KIWISAVERBALANCE_FIELD("currentKiwiSaverBalance"),
	VOLUNTARY_CONTRIBUTIONVALUE_FIELD("voluntaryContributionValue"),
	VOLUNTARY_CONTRIBUTIONFREQUENCY_FIELD("voluntaryContributionFrequency"),
	RISK_FIELD("risk"),
	SAVINGSGOAL_FIELD("savingsGoal");
	
	private final String field;
	 DataFields(String field) {
		this.field = field;
	}
	public String field() {
		return this.field;
	}
}