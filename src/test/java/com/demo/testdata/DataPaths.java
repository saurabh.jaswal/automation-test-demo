package com.demo.testdata;

public class DataPaths {
	public static final String STORY_ONE="\\testData\\storyOne";
	public static final String STORY_TWO="\\testData\\storyTwo";
	public static final String JSON="JSON";
	public static final String AGE_HELPINFO="ageHelpInfo";
	public static final String EMP_STATUS_INFO="empStatusInfo";
	public static final String PIRRATE_INFO="pirRateInfo";
	public static final String KIWI_SAVERBALANCE_INFO="kiwiSaverBalanceInfo";
	public static final String KIWI_SAVER_VALUE_3="3";
	public static final String KIWI_SAVER_VALUE_4="4";
	public static final String KIWI_SAVER_VALUE_8="8";
	public static final String VC_FREQUENCY_VALUE_FORTNIGHTLY="Fortnightly";
	public static final String VC_FREQUENCY_VALUE_ONEOFF="One-off";
	public static final String VC_FREQUENCY_VALUE_WEEKLY="Weekly";
	public static final String VC_FREQUENCY_VALUE_MONTHLY="Monthly";
	public static final String VC_FREQUENCY_VALUE_ANNUALLY="Annually";
	public static final String PIR_RATE_LOW="10.5";
	public static final String PIR_RATE_MEDIUM="17.5";
	public static final String PIR_RATE_HIGH="28";
	public static final String RISK_PROFILE_LOW="low";
	public static final String RISK_PROFILE_MEDIUM="medium";
	public static final String RISK_PROFILE_HIGH="high";
}
